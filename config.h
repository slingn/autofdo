/* config.h.  Static configuration for CMake-based Razor autofdo - modify as needed. */

/* define if the LLVM library is available */
#define HAVE_LLVM 1

/* Name of package */
#define PACKAGE "autofdo"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "autofdo@googlegroups.com"

/* Define to the full name of this package. */
#define PACKAGE_NAME "AutoFDO"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "AutoFDO 0.14"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "autofdo"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "0.14"

/* Version number of package */
#define VERSION "0.14"

/* Stops putting the code inside the Google namespace */
#define _END_GOOGLE_NAMESPACE_ }

/* Puts following code inside the Google namespace */
#define _START_GOOGLE_NAMESPACE_ namespace google {
