#ifndef WINDOWS_MMAP_H
#define WINDOWS_MMAP_H

#include <BaseTsd.h>
#include <io.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * mmap() flags
 */
#define PROT_READ     0x1
#define PROT_WRITE    0x2
/* This flag is only available in WinXP+ */
#ifdef FILE_MAP_EXECUTE
#define PROT_EXEC     0x4
#else
#define PROT_EXEC        0x0
#define FILE_MAP_EXECUTE 0
#endif

#define MAP_FILE      0x00
#define MAP_SHARED    0x01
#define MAP_PRIVATE   0x02
#define MAP_ANONYMOUS 0x20
#define MAP_ANON      MAP_ANONYMOUS
#define MAP_FAILED    ((void *) -1)

/*
 * msync() flags
 */
#define MS_ASYNC        0x0001  /* return immediately */
#define MS_INVALIDATE   0x0002  /* invalidate all cached data */
#define MS_SYNC         0x0010  /* msync synchronously */

/*
 * flock() operations
 */
#define   LOCK_SH   1    /* shared lock */
#define   LOCK_EX   2    /* exclusive lock */
#define   LOCK_NB   4    /* don't block when locking */
#define   LOCK_UN   8    /* unlock */

void *mmap(void *start, size_t length, int prot, int flags, int fd,
           off_t offset);

void munmap(void *addr, size_t length);

int msync(void *addr, size_t length, int flags);

int flock(int fd, int operation);

#ifdef __cplusplus
}
#endif

#endif /* WINDOWS_MMAP_H */