cmake_minimum_required(VERSION 2.8.12)
project(autofdo)

find_package(LLVM REQUIRED CONFIG)

include(CheckCXXCompilerFlag)
include(ExternalProject)

## Default to Release build
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release CACHE STRING
       "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel."
       FORCE)
endif()

include_directories(${CMAKE_CURRENT_BINARY_DIR}/include)
link_directories(${CMAKE_CURRENT_BINARY_DIR}/lib)

if (MSVC)
  # Force unsigned char.
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /J")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /J")
  add_definitions(-DCOMPILER_MSVC)
  # Don't overload min() and max()
  add_definitions(-DNOMINMAX)
  # Don't warn about sprintf(), etc.
  add_definitions(-D_CRT_SECURE_NO_WARNINGS)
  # warning C4996: 'std::_Fill_n': Function call with parameters that may be unsafe - this call relies on the caller to check that the passed values are correct.
  add_definitions(-D_SCL_SECURE_NO_WARNINGS)
  option(RAZOR "Enable support for parsing Razor profiles" ON)
else()
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
  option(RAZOR "Enable support for parsing Razor profiles" OFF)
endif()

if (RAZOR)
  add_definitions(-DRAZOR)
endif()

################################################################################
# llvm - http://llvm.org/releases/download.html
################################################################################
include_directories(${LLVM_INCLUDE_DIRS})
add_definitions(${LLVM_DEFINITIONS})

# Find the libraries that correspond to the LLVM components
# that we wish to use
llvm_map_components_to_libnames(llvm_libs support core profiledata)

################################################################################
# elfio - http://elfio.sourceforge.net/
################################################################################
#set(elfio_RELEASE Master)
#ExternalProject_Add(
#        elfio-${elfio_RELEASE}
#        PREFIX ${CMAKE_CURRENT_BINARY_DIR}/elfio-${elfio_RELEASE}
#        SOURCE_DIR ${CMAKE_SOURCE_DIR}/elfio
#        CONFIGURE_COMMAND ${CMAKE_SOURCE_DIR}/elfio/configure --prefix=${CMAKE_CURRENT_BINARY_DIR}
#        BUILD_COMMAND make
#        INSTALL_COMMAND make install PREFIX=${CMAKE_CURRENT_BINARY_DIR}
#        PATCH_COMMAND ""
#)
include_directories(${CMAKE_SOURCE_DIR}/elfio)

################################################################################
# gflags - https://github.com/gflags/gflags
################################################################################
set(gflags_RELEASE 2.1.2)
ExternalProject_Add(
        gflags-${gflags_RELEASE}
        PREFIX ${CMAKE_CURRENT_BINARY_DIR}/gflags-${gflags_RELEASE}
        SOURCE_DIR ${CMAKE_SOURCE_DIR}/gflags
        CMAKE_ARGS -DBUILD_SHARED_LIBS=OFF -DCMAKE_INSTALL_PREFIX=${CMAKE_CURRENT_BINARY_DIR}
        PATCH_COMMAND ""
)

if (MSVC)
  # Don't use __declspec(dllexport|dllimport) if this is a static build
  add_definitions(-DGFLAGS_DLL_DECLARE_FLAG= -DGFLAGS_DLL_DEFINE_FLAG=)
endif()

################################################################################
# glog - https://github.com/google/glog
################################################################################
set(glog_RELEASE Master)
ExternalProject_Add(
        glog-${glog_RELEASE}
        PREFIX ${CMAKE_CURRENT_BINARY_DIR}/glog-${glog_RELEASE}
        SOURCE_DIR ${CMAKE_SOURCE_DIR}/glog
        CMAKE_ARGS -DBUILD_SHARED_LIBS=OFF -DWITH_GFLAGS=OFF -DCMAKE_INSTALL_PREFIX=${CMAKE_CURRENT_BINARY_DIR}
        PATCH_COMMAND ""
)

if (MSVC)
  # Don't use __declspec(dllexport|dllimport) if this is a static build
  add_definitions(-DGOOGLE_GLOG_DLL_DECL=)
endif()

################################################################################
# internal libraries
################################################################################

file (GLOB libquipper_SRC
        chromiumos-wide-profiling/address_mapper.cc
        chromiumos-wide-profiling/perf_reader.cc
        chromiumos-wide-profiling/perf_parser.cc
        chromiumos-wide-profiling/utils.cc
        chromiumos-wide-profiling/buffer_reader.cc
        chromiumos-wide-profiling/data_reader.cc
        chromiumos-wide-profiling/buffer_writer.cc
        chromiumos-wide-profiling/data_writer.cc
        )
add_library(quipper STATIC ${libquipper_SRC})
add_dependencies(quipper glog-${glog_RELEASE})

if (!MSVC)
  target_compile_features(quipper PUBLIC cxx_constexpr)
endif()

file (GLOB libsymbolize_SRC
        symbolize/addr2line_inlinestack.cc
        symbolize/bytereader.cc
        symbolize/dwarf2reader.cc
        symbolize/dwarf3ranges.cc
        symbolize/elf_reader.cc
        symbolize/functioninfo.cc
        )
if(WIN32)
  list(APPEND libsymbolize_SRC symbolize/WindowsMMap.c)
endif()
add_library(symbolize STATIC ${libsymbolize_SRC})
set_target_properties(symbolize PROPERTIES COMPILE_FLAGS -DUSE_ELFIO)
add_dependencies(symbolize glog-${glog_RELEASE})

target_compile_features(symbolize PUBLIC cxx_nullptr)

file (GLOB libmd5_SRC
        md5/md5.cc
        )
add_library(md5 STATIC ${libmd5_SRC})

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

if(RAZOR)
  file (GLOB libsqlite3_SRC
          sqlite3/sqlite3.c
  )
  add_library(sqlite3 STATIC ${libsqlite3_SRC})
  include_directories(${CMAKE_SOURCE_DIR}/sqlite3)

  file (GLOB librazor_SRC
          razor/razor_db_parser.cpp
  )
  add_library(razor STATIC ${librazor_SRC})
  add_dependencies(razor sqlite3 glog-${glog_RELEASE})
endif()

################################################################################
# tools
################################################################################

file (GLOB objects_SRC
        addr2line.cc
        gcov.cc
        instruction_map.cc
        llvm_profile_writer.cc
        module_grouper.cc
        profile_creator.cc
        profile_writer.cc
        sample_reader.cc
        source_info.cc
        symbol_map.cc
        profile.cc
        )
set (COMMON_DEPENDENCIES gflags-${gflags_RELEASE} glog-${glog_RELEASE})
set (COMMON_LINK_LIBRARIES quipper symbolize md5 gflags glog ${llvm_libs})
if (RAZOR)
  list(APPEND COMMON_LINK_LIBRARIES razor sqlite3)
  if ("${CMAKE_SYSTEM}" MATCHES "Linux")
    list(APPEND COMMON_LINK_LIBRARIES dl)
  endif()
endif()

if (WIN32)
  list(APPEND COMMON_LINK_LIBRARIES shlwapi.lib)
else()
  list(APPEND COMMON_LINK_LIBRARIES pthread)
endif()

add_executable(create_gcov ${objects_SRC} create_gcov.cc)
add_dependencies(create_gcov ${COMMON_DEPENDENCIES})
target_link_libraries(create_gcov ${COMMON_LINK_LIBRARIES})

add_executable(create_llvm_prof ${objects_SRC} create_llvm_prof.cc)
add_dependencies(create_llvm_prof ${COMMON_DEPENDENCIES})
target_link_libraries(create_llvm_prof ${COMMON_LINK_LIBRARIES})

add_executable(profile_diff ${objects_SRC} profile_reader.cc profile_diff.cc)
add_dependencies(profile_diff ${COMMON_DEPENDENCIES})
target_link_libraries(profile_diff ${COMMON_LINK_LIBRARIES})

add_executable(profile_merger ${objects_SRC} profile_reader.cc profile_merger.cc)
add_dependencies(profile_merger ${COMMON_DEPENDENCIES})
target_link_libraries(profile_merger ${COMMON_LINK_LIBRARIES})

add_executable(profile_update ${objects_SRC} profile_reader.cc profile_update.cc)
add_dependencies(profile_update ${COMMON_DEPENDENCIES})
target_link_libraries(profile_update ${COMMON_LINK_LIBRARIES})

add_executable(sample_merger ${objects_SRC} sample_merger.cc)
add_dependencies(sample_merger ${COMMON_DEPENDENCIES})
target_link_libraries(sample_merger ${COMMON_LINK_LIBRARIES})

file (GLOB dump_gcov_SRC
        profile_reader.cc
        symbol_map.cc
        module_grouper.cc
        gcov.cc
        dump_gcov.cc
        )
add_executable(dump_gcov ${dump_gcov_SRC})
add_dependencies(dump_gcov ${COMMON_DEPENDENCIES})
target_link_libraries(dump_gcov ${COMMON_LINK_LIBRARIES})
