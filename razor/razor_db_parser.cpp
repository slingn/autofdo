#include "razor_db_parser.h"

extern "C" {
#include <sqlite3.h>
}

#include <sstream>
#include <stdexcept>

#include "base/logging.h"

namespace razor {

ostream& operator<< (ostream &out, Module &module)
{
    out << "Module - " << module.name << " [" << module.moduleId << "]" << std::endl;
    out << "\tpath: " << module.path << std::endl;
    out << "\ttime: " << module.time << std::endl;
    out << "\tprxName: " << module.prxName << std::endl;
    out << "\tprxVersion: " << module.prxVersion << std::endl;

    return out;
}

ostream& operator<< (ostream &out, Symbol &symbol)
{
    out << "Symbol - " << symbol.name << " @ 0x" << std::hex << symbol.address << "-0x" << (symbol.address + symbol.size) << " [" << symbol.moduleId << "]" << std::endl;
    out << "\tsrcFile: " << symbol.srcFile << std::endl;

    return out;
}

RazorDbParser::RazorDbParser(const std::string &profile_file) :
        db(NULL),
        symbolRangeMap(),
        symbolNameAddressMap(),
        moduleIdMap(),
        addressHitCountMap()
{
    int rc;

    rc = sqlite3_open(profile_file.c_str(), &db);
    if (rc) {
        LOG(ERROR) << "Failed to open profile database '" << profile_file << "': " << sqlite3_errmsg(db);
        sqlite3_close(db);
        db = NULL;
    }
}

RazorDbParser::~RazorDbParser()
{
    if (db) {
        sqlite3_close(db);
        db = NULL;
    }
}

const AddressHitCountMap &RazorDbParser::GetAddressHitCountMap()
{
    return addressHitCountMap;
}

const Symbol *RazorDbParser::GetSymbolForAddress(uint64_t address)
{
    SymbolRangeMap::Iterator itr = symbolRangeMap.Find(address);
    if (itr == symbolRangeMap.End()) {
        return NULL;
    }
    return &(itr->second);
}

const Symbol *RazorDbParser::FindSymbolWithName(const std::string &name)
{
    SymbolNameAddressMap::iterator itr = symbolNameAddressMap.find(name);
    if (itr == symbolNameAddressMap.end()) {
        return NULL;
    }
    return GetSymbolForAddress(itr->second);
}

const Module *RazorDbParser::GetModuleForAddress(uint64_t address)
{
    const Symbol *symbol = GetSymbolForAddress(address);
    if (!symbol) {
        return NULL;
    }
    ModuleIdMap::iterator itr = moduleIdMap.find(symbol->moduleId);
    if (itr == moduleIdMap.end()) {
        return NULL;
    }
    return &(itr->second);
}

bool RazorDbParser::_ReadModules()
{
    sqlite3_stmt *stmt = NULL;
    int rc;

    rc = sqlite3_prepare_v2(db, "SELECT ModuleId, ModuleName, ModulePath, ModuleTime, PrxName, PrxVersion"
            " FROM Module", -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "ERROR: %s\n", sqlite3_errmsg(db));
        return false;
    }

    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        ModuleId moduleId = sqlite3_column_int(stmt, 0);
        moduleIdMap.insert(std::make_pair(moduleId, Module(moduleId,
                                         (char *)sqlite3_column_text(stmt, 1),
                                         (char *)sqlite3_column_text(stmt, 2),
                                         (unsigned int)sqlite3_column_int(stmt, 3),
                                         (char *)sqlite3_column_text(stmt, 4),
                                         (unsigned int)sqlite3_column_int(stmt, 5))));
    }

    if (rc != SQLITE_DONE) {
        LOG(ERROR) << "Failed to read modules - " << sqlite3_errmsg(db);
    }
    sqlite3_finalize(stmt);
    return rc == SQLITE_DONE;
}

bool RazorDbParser::_ReadSymbols()
{
    sqlite3_stmt *stmt = NULL;
    int rc;

    rc = sqlite3_prepare_v2(db, "SELECT FunctionAddress, FunctionSize, ModuleId, FunctionName, SourceFileName"
            " FROM Function", -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "ERROR: %s\n", sqlite3_errmsg(db));
        return false;
    }

    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        uint64_t symbolAddress = sqlite3_column_int64(stmt, 0);
        uint64_t symbolSize = sqlite3_column_int64(stmt, 1);
        ModuleId moduleId = (ModuleId)sqlite3_column_int(stmt, 2);
        char *symbolName = (char *)sqlite3_column_text(stmt, 3);
        char *symbolSrcFile = (char *)sqlite3_column_text(stmt, 4);

        symbolRangeMap.InsertRange(symbolAddress, symbolAddress + symbolSize, Symbol(
                symbolAddress,
                symbolSize,
                moduleId,
                symbolName,
                symbolSrcFile)
        );
        symbolNameAddressMap.insert(std::make_pair(symbolName, symbolAddress));
    }

    if (rc != SQLITE_DONE) {
        LOG(ERROR) << "Failed to read symbols - " << sqlite3_errmsg(db);
    }
    sqlite3_finalize(stmt);
    return rc == SQLITE_DONE;
}

bool RazorDbParser::_ReadHitCounts()
{
    sqlite3_stmt *stmt = NULL;
    int rc;

    rc = sqlite3_prepare_v2(db, "SELECT Address, HitCount"
            " FROM HitCount", -1, &stmt, NULL);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "ERROR: %s\n", sqlite3_errmsg(db));
        return false;
    }

    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        uint64_t address = sqlite3_column_int64(stmt, 0);
        uint64_t hitCount = sqlite3_column_int64(stmt, 1);

        addressHitCountMap.insert(std::make_pair(address, hitCount));
    }

    if (rc != SQLITE_DONE) {
        LOG(ERROR) << "Failed to read hit counts - " << sqlite3_errmsg(db);
    }
    sqlite3_finalize(stmt);
    return rc == SQLITE_DONE;
}

bool RazorDbParser::ReadFile()
{
    return db != NULL && _ReadModules() && _ReadSymbols() && _ReadHitCounts();
}

}