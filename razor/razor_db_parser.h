#ifndef AUTOFDO_RAZOR_DB_PARSER_H
#define AUTOFDO_RAZOR_DB_PARSER_H

#include <map>
#include <unordered_map>

extern "C" {
#include <sqlite3.h>
}

#include "symbolize/nonoverlapping_range_map.h"

namespace razor {

typedef unsigned int ModuleId;

class Module {
public:
    Module(ModuleId moduleId_, char *name_, char *path_, unsigned int time_, char *prxName_, unsigned int prxVersion_) :
      moduleId(moduleId_),
      name(name_),
      path(path_),
      time(time_),
      prxName(prxName_),
      prxVersion(prxVersion_)
    {}
    ~Module()
    {}

    ModuleId moduleId;
    std::string name;
    std::string path;
    unsigned int time;
    std::string prxName;
    unsigned int prxVersion;
};

ostream& operator<< (ostream &out, Module &module);

class Symbol {
public:
    Symbol(const uint64_t address_, const uint64_t size_, const ModuleId moduleId_, const std::string &name_, const std::string srcFile_) :
      address(address_),
      size(size_),
      moduleId(moduleId_),
      name(name_),
      srcFile(srcFile_)
    {}

    uint64_t address;
    uint64_t size;
    ModuleId moduleId;
    std::string name;
    std::string srcFile;
};

ostream& operator<< (ostream &out, Symbol &symbol);

typedef std::map<ModuleId, Module> ModuleIdMap;
typedef autofdo::NonOverlappingRangeMap<Symbol> SymbolRangeMap;
typedef std::unordered_map<std::string, uint64_t> SymbolNameAddressMap;
typedef std::unordered_map<uint64_t, uint64_t> AddressHitCountMap;

class RazorDbParser {
public:
    RazorDbParser(const std::string &profile_file);
    ~RazorDbParser();

    bool ReadFile();

    const AddressHitCountMap &GetAddressHitCountMap();

    const Symbol *GetSymbolForAddress(uint64_t address);
    const Symbol *FindSymbolWithName(const std::string &name);
    const Module *GetModuleForAddress(uint64_t address);

private:
    bool _ReadModules();
    bool _ReadSymbols();
    bool _ReadHitCounts();

    sqlite3 *db;

    SymbolRangeMap symbolRangeMap;
    SymbolNameAddressMap symbolNameAddressMap;
    ModuleIdMap moduleIdMap;
    AddressHitCountMap addressHitCountMap;

    DISALLOW_COPY_AND_ASSIGN(RazorDbParser);
};

}

#endif // AUTOFDO_RAZOR_DB_PARSER_H
